# ehealth-consulting-stores-backend

Voraussetzungen:
- Node + npm (Die Version bitte der Datei "package.json" entnehmen)
- Anaconda
    - Link zur Installation: https://www.anaconda.com/products/individual

## Erstellen eines lokalen Backends
Alle Befehle im Root-Verzeichnis des Repositories ausführen.

### Projekt Dependencies installieren
`npm install`

### Global Dependencies installieren
Prisma:

`npm install -g prisma@1.34`

Weitere Globale abhängigkeiten:

`npm install -g graphql-cli`

`npm install -g graphqlgen`

### .env File
`.env` File zu konfiguration der Umgebungsvariablen anlegen im Root-Verzeichnis

Inhalt:

```json
PRISMA_ENDPOINT=https://ehealth-backend-deb075da3f.herokuapp.com/ehealth-consulting-store-backend/dev
PRISMA_SECRET=my_secret
SERVER_PORT=4000

TOKEN_EXPIRY_TIME=1d
TOKEN_SECRET=my_token_secret

EMAIL_ADDRESS=my_email@my_email.de
EMAIL_CIPHERS=SSLv3
EMAIL_PORT=587
EMAIL_REJECT=false
EMAIL_SERVICE=mail.my_email.de
PASSWORD_EMAIL=my_email_password
USER_EMAIL=my_email@my_email.de
VERIFICATIONCODELENGTH=200

LOGIN_LINK=http://localhost:4200/login

SAVE_FOLDER_NAME=models
SAVE_FILE_NAME=buffer

GAMMA=0.99
TAU=0.05
ALPHA_HIGH=0.5
ALPHA_LOW=0.01
ALPHA=0.5
EPS=0.2
EPS_LOW=0.001
DELTA=0.001
```

### Datenspeicherung
Ein Ordner mit der Bezeichnung "models" (je nach Spezifizierung der Variable SAVE_FOLDER_NAME in der .env Datei) muss erstellt werden, bevor der Server zum ersten mal gestartet wird. Hier wird der Gedächtnisspeicher und die Gewichtungen des neuronalen Netzes abgelegt.

### Dev mode mit Change Detection Starten
`npm run dev`

Die API des Backends ist nach dem Start erreichbar unter : http://localhost:4011/

> Weitere Optionen:
>#### Server builden und starten
>`npm start`

## Deployed Backend Server
Der Backend Server wurde bei Heroku deployed und ist [hier](https://ehealth-consulting-backend.herokuapp.com/graphql)
erreichbar

## Training und Validierung des Agenten
Im Ordner ./training_validation/ existieren Jupyter-Notebooks für das Training und die Validation des Agenten. Das Training und die Validation können mit dem Start von `jupyter-notebook` und der Auswahl im Reitermenü `Cell -> Run All` ausgeführt werden.