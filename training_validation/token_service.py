import requests

class TokenService:
    __header_query = """
        query($email: String!, $password: String!) {
        login(email:$email, password:$password){
            token
        }
        }
        """

    def __init__(self, url):
        self.__url = url
        pass

    def login_user(self, email, password):
        if email is None or password is None:
            raise TypeError
        variables = {'email': email, 'password': password}
        req = requests.post(url=self.__url, json={'query': TokenService.__header_query, 'variables': variables})
        token = req.json()['data']['login']['token']
        return token