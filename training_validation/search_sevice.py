import requests

class SearchService:
    __start_session_query = """
        query($topic: String!, $skip: Int!, $limit: Int!) {
        startSearchSession(data: {
            topic: $topic,
            skip: $skip,
            limit: $limit
        }) {
            id
            email
            forename
            lastname
        }
        }
        """
    
    __continue_session_query = """
        query($topic: String!, $skip: Int!, $limit: Int!) {
        continueSearchSession(data: {
            topic: $topic,
            skip: $skip,
            limit: $limit
        }) {
            id
            email
            forename
            lastname
        }
        }
        """

    __success_session_query = """
        query($topic: String!, $skip: Int!, $limit: Int!) {
        successSearchSession(data: {
            topic: $topic,
            skip: $skip,
            limit: $limit
        })
        }
        """

    __cancel_session_query = """
        query($topic: String!, $skip: Int!, $limit: Int!) {
        cancelSearchSession(data: {
            topic: $topic,
            skip: $skip,
            limit: $limit
        })
        }
        """

    def __init__(self, url):
        self.__url = url
        pass

    def set_token(self, token):
        self.__token_headers = {'Authorization': token}
        pass

    def _start_query(self, query, topic, skip, limit):
        if self.__token_headers is None:
            raise TypeError
        variables = {'topic': topic, 'skip': skip, 'limit': limit}
        req = requests.post(url=self.__url, json={'query': query, 'variables': variables}, headers=self.__token_headers)
        return req

    def start_search(self, topic, skip, limit):
        req = self._start_query(SearchService.__start_session_query, topic=topic, skip=skip, limit=limit)
        return req
    
    def continue_search(self, topic, skip, limit):
        req = self._start_query(SearchService.__continue_session_query, topic=topic, skip=skip, limit=limit)
        return req
    
    def succeed_search(self, topic, skip, limit):
        req = self._start_query(SearchService.__success_session_query, topic=topic, skip=skip, limit=limit)
        return req

    def cancel_search(self, topic, skip, limit):
        req = self._start_query(SearchService.__cancel_session_query, topic=topic, skip=skip, limit=limit)
        return req