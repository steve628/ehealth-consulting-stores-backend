from token_service import TokenService
from search_sevice import SearchService

url = 'http://localhost:4000/graphql'
service_token = TokenService(url)
service_search = SearchService(url)

running = True
count = 0
MAX_COUNT = 10
topic = 'Machine Learning'
lastname = 'Miason'

token = service_token.login_user('steve62813@gmail.com', '123456')
service_search.set_token(token)

while running:
    req = service_search.start_search(topic, 0, 5)
    if req.status_code != 200:
        running = False
        break

    req = service_search.continue_search(topic, 5, 5)
    if req.status_code != 200:
        running = False
        break

    req = service_search.continue_search(topic, 10, 5)
    if req.status_code == 200:
        try:
            result = req.json()['data']['continueSearchSession'][0]['lastname']
        except TypeError as err:
            break
    
    print('Lastname: ', result)
    if result == lastname:
        print('Hits: ', count + 1)
        req = service_search.succeed_search(topic, 10, 5)
        if req.status_code != 200:
            running = False
            break
        count += 1
    else:
        req = service_search.cancel_search(topic, 10, 5)
        if req.status_code != 200:
            running = False
            break
        count = 0
    
    if count >= MAX_COUNT:
        running = False
        print('Successfully trained')