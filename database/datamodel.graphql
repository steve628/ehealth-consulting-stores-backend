type User {
    id: ID! @id
    email: String! @unique
    password: String!
    forename: String
    lastname: String
    picture: String
    price: Float
    phoneNumber: String
    role: Role @default(value: USER)
    description: String
    verificationCode: String
    active: Boolean @default(value: false)
    services: [Service!]! @relation(name: "ServiceOffer", onDelete: CASCADE)
    topics: [Topic!]! @relation(name: "UserRelatedTo", onDelete: SET_NULL)
    writtenReviews: [Review!]! @relation(name: "ReviewAuthor", onDelete: SET_NULL)
    ratedReviews: [Review!]! @relation(name: "RatedConsultant", onDelete: CASCADE)
    messages: [Message!]! @relation(name: "WriteMessage", onDelete: CASCADE)
    favoriteConsultants: [User!]! @relation(name: "FavouriteConsultant", onDelete: SET_NULL)
    favoriteServices: [Service!]! @relation(name: "FavoriteService", onDelete: SET_NULL)
    consultants: [User!]! @relation(name: "AdvisedBy", onDelete: SET_NULL)
}

type Category {
    id: ID! @id
    name: String! @unique
    subCategories: [SubCategory!]! @relation(name: "CategoryConsists", onDelete: CASCADE)
}

type SubCategory {
    id: ID! @id
    name: String! @unique
    category: Category! @relation(name: "CategoryConsists", onDelete: SET_NULL)
    services: [Service!]! @relation(name: "SubCategoryProvide", onDelete: SET_NULL)
}

type Service {
    id: ID! @id
    title: String!
    description: String
    picture: String
    video: String
    subCategory: SubCategory @relation(name: "SubCategoryProvide", onDelete: SET_NULL)
    topics: [Topic!]! @relation(name: "ServiceRelatedTo", onDelete: SET_NULL)
    favoredUsers: [User!]! @relation(name: "FavoriteService", onDelete: SET_NULL)
    owner: User! @relation(name: "ServiceOffer", onDelete: SET_NULL)
}

type Topic {
    id: ID! @id
    name: String @unique
    services: [Service!]! @relation(name: "ServiceRelatedTo", onDelete: SET_NULL)
    users: [User!]! @relation(name: "UserRelatedTo", onDelete: SET_NULL)
}

type Review {
    id: ID! @id
    text: String!
    rating: Int!
    author: User! @relation(name: "ReviewAuthor", onDelete: SET_NULL)
    ratedConsultant: User! @relation(name: "RatedConsultant", onDelete: SET_NULL)
}

type Message {
    id: ID! @id
    text: String!
    createdAt: DateTime! @createdAt
    updatedAt: DateTime! @updatedAt
    chat: Chat! @relation(name: "ChatBelongTo", onDelete: SET_NULL)
    author: User! @relation(name: "WriteMessage", onDelete: SET_NULL)
}

type Chat {
    id: ID! @id
    messages: [Message!]! @relation(name: "ChatBelongTo", onDelete: CASCADE)
}
