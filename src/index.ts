import { ApolloServer, gql } from 'apollo-server-express';
import * as express from 'express';
import * as http from 'http';
import { prisma } from './generated/prisma-client';
import { importSchema } from 'graphql-import';
import { operationAuthorized, verifyToken } from './authorization/authorization';
import { resolvers } from './resolvers/resolvers';
import { SERVER_PORT } from './config/env.config';
import { mailRouter } from './email';

const PORT = (process.env.PORT) ? process.env.PORT : SERVER_PORT;
const app = express();
const server: ApolloServer = new ApolloServer({
    typeDefs: gql(importSchema('./src/schema.graphql')),
    //@ts-ignore
    resolvers: resolvers,
    context: ({req, connection,res}) => {
        let header = { authToken: '' };
        if (connection) {
            header = connection.context;
        } else {
            header.authToken = req.headers.authorization;
        }

        const authRequired = (connection) ? operationAuthorized(connection.query) : operationAuthorized(req.body.query);

        return {
            db: prisma,
            req: req,
            userId: verifyToken(header, authRequired)
        };
    },
    subscriptions: {
        onConnect: (connectionParams: any) => {
            console.log('Connection initiated');
            return { authToken: connectionParams.Authorization };
        },
        onDisconnect: () => {
            console.log('Web Socket were disconnected');
        }
    },
    playground: true,
    introspection: true
});

app.use("/email", mailRouter);

server.applyMiddleware({ app });
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({port: PORT}, () => {
    console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`);
    console.log(`🚀 Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
});
