import {
    loadLayersModel,
    Tensor1D,
    Rank,
    tensor2d,
    ones,
    zeros,
    scalar,
    range,
    exp,
    sum,
    concat,
    any,
    Tensor,
} from '@tensorflow/tfjs-node';
import { QFunctionAnn } from './q-function-ann';
import { AgentMemory } from './agent-memory';
import { MathHelper } from '../helper/math-helper';
import { SAVE_FOLDER_NAME } from '../config/env.config';
import {
    eps,
    epsLow,
    alphaLow,
    alphaHigh,
    delta
} from '../learning/learning-paramter';

export class SearchEngineAgent {
    private totalReward = 0;
    private memory = new AgentMemory();
    private qFunction: QFunctionAnn;

    constructor(
        stateDimension: number,
        private actionRange,
        private gamma = 0.8,
        private eps = 0.01,
        private tau = 0.1,
        private alpha = 0.5,
        private learnBatch = undefined,
        networkArc = [100],
    ) {
        this.qFunction = new QFunctionAnn(stateDimension, networkArc);
    }

    public set TotalReward(val: number) {
        this.totalReward = val;
    }

    public get TotalReward(): number {
        return this.totalReward;
    }

    public setState(state: Tensor1D): void {
        this.memory.addState(state);
    }

    public resetMemory(): void {
        this.memory.clearCurrentMemory();
    }

    public setReward(reward: number): void {
        this.totalReward += reward;
        this.memory.Reward = reward;
    }


    public getAction(observation: Tensor1D): number {
        const a = this.chooseAction(observation);
        const action = this.actionRange[a];
        this.memory.Action = action;
        return action;
    }

    private chooseAction(obs: Tensor1D): number {
        let chosenAction: number;
        let observation = obs;
        if (!observation || Boolean(any(observation.isNaN()).arraySync())) {
            observation = this.memory.State;
        }
        if (Math.random() < this.eps) {
            chosenAction = Math.floor(Math.random() * this.actionRange.length);
            return chosenAction;
        }
        const buffer = zeros([this.actionRange.length]).bufferSync();
        for (let i = 0; i < this.actionRange.length; i++) {
            buffer.set((this.qFunction.predict(observation, this.actionRange[i]) as Tensor<Rank>).arraySync()[0], i);
        }
        let qValues = buffer.toTensor();
        const toChoose = range(0, this.actionRange.length);
        qValues = qValues.div(scalar(this.tau)).sub(qValues.div(scalar(this.tau)).max());
        const pw = exp(qValues).div(sum(exp(qValues)));
        if (Boolean(any(pw.isNaN()).arraySync()) || Boolean(any(pw.isInf()).arraySync())) {
            chosenAction = Math.floor(Math.random() * qValues.size);
        } else {
            chosenAction = MathHelper.randomChoise(pw.as1D().arraySync());
        }
        return chosenAction;
    }

    public async learn(): Promise<void> {
        const { state, action, reward, nextState } = this.memory.getBatch(this.learnBatch);
        const qsa = (this.qFunction.predict(state, action) as Tensor<Rank>).squeeze();
        let maxQValue = tensor2d([], [reward.shape[0], 0]);
        let qValue: Tensor<Rank> | Tensor<Rank>[];
        for (const a of this.actionRange) {
            qValue = this.qFunction.predict(nextState, ones([reward.shape[0], 1]).mul(scalar(a)));
            maxQValue = concat([qValue as Tensor<Rank.R2>, maxQValue], 1);
        }
        const maxQ = maxQValue.max(1).squeeze();
        const Y = scalar(1 - this.alpha)
            .mul(qsa)
            .add(scalar(this.alpha))
            .mul(reward.squeeze().add(scalar(this.gamma)).mul(maxQ));

        if (Boolean(Y.abs().max().greater(scalar(100)).arraySync())) {
            console.log('Warning Q-Function seems to diverge! Max Value = ');
            Y.abs().max().print();
        }
        await this.qFunction.fit(state, action, Y);
    }

    public async restore(name: string): Promise<void> {
        this.loadMemory(name);
        await this.loadQFunction();
    }

    public async save(name: string): Promise<void> {
        this.saveMemory(name);
        await this.saveQFunction();
    }

    public saveMemory(name: string): void {
        this.memory.saveMemory(name);
    }

    public loadMemory(name: string): void {
        this.memory.loadMemory(name);
    }

    public async saveQFunction(): Promise<void> {
        await this.qFunction.QFunction.save(`file://${SAVE_FOLDER_NAME}`);
    }

    public async loadQFunction(): Promise<void> {
        try {
            this.qFunction.QFunction = await loadLayersModel(`file://${SAVE_FOLDER_NAME}/model.json`);
        } catch (e) {
            console.log('Model save file not found');
        }
    }

    public adjustLearning(tenency: number): void {
        if (tenency > 0) {
            this.alpha = Math.max(this.alpha - delta, alphaLow);
            this.eps = Math.max(this.eps - delta, epsLow);
        } else if (tenency < 0) {
            this.alpha = Math.min(this.alpha + delta, alphaHigh);
            this.eps = Math.min(this.eps + delta, eps);
        }
    }
}
