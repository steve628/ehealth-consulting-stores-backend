import {
    ALPHA,
    EPS,
    EPS_LOW,
    DELTA,
    GAMMA,
    TAU,
    ALPHA_LOW,
    ALPHA_HIGH
} from '../config/env.config';

const gamma = GAMMA ? GAMMA : 0.99;
const tau = TAU ? TAU : 0.05;
const alpha = ALPHA ? ALPHA : 0.5;
const alphaLow = ALPHA_LOW ? ALPHA_LOW : 0.1;
const alphaHigh = ALPHA_HIGH ? ALPHA_HIGH : alpha;
const eps = EPS ? EPS : 0.2;
const epsLow = EPS_LOW ? EPS_LOW : 0.001;
const delta = DELTA ? DELTA : 0.001;
const networkArc = [100, 50, 20];

export {
    gamma,
    tau,
    alpha,
    alphaLow,
    alphaHigh,
    eps,
    epsLow,
    delta,
    networkArc
};
