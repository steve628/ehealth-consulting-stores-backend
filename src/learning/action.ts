export enum Action {
    price_ASC,
    price_DESC,
    forename_ASC,
    forename_DESC,
    description_ASC,
    description_DESC
}

export const getAllActionKeys = (): string[] => {
    return Object.values(Action).filter(elem => isNaN(Number(elem))) as string[];
};

export const getAllActionValues = (): number[] => {
    return Object.values(Action).filter(elem => !isNaN(Number(elem))) as number[];
};
