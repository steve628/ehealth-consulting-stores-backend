import {
    LayersModel,
    Sequential,
    layers,
    regularizers,
    EarlyStopping,
    losses,
    concat,
    tensor1d,
    zeros,
    Tensor2D,
    Tensor,
    Rank
} from '@tensorflow/tfjs-node';
import { MathHelper } from '../helper/math-helper';

export class QFunctionAnn {
    public unfitted: boolean;
    private qFunction: LayersModel;

    constructor(
        private stateDimension: number,
        networkArc = [100],
        l2Reg = 0.001,
    ) {
        this.unfitted = true;

        this.qFunction = this.initModel(stateDimension, networkArc, l2Reg);

        this.compileQFunction();

        this.qFunction.summary();
    }

    public get QFunction() {
        return this.qFunction;
    }

    public set QFunction(val) {
        this.qFunction = val;
        this.compileQFunction();
    }

    private initModel(stateDimension: number ,networkArc: number[], l2Reg: number): LayersModel {
        const model = new Sequential();

        model.add(layers.dense({
            units: networkArc[0],
            kernelRegularizer: regularizers.l2({ l2: l2Reg }),
            inputDim: stateDimension + 1,
            activation: 'tanh'
        }));

        model.add(layers.batchNormalization());

        for (let i = 1; i < networkArc.length; i++) {
            model.add(layers.dense({
                units: networkArc[i],
                kernelRegularizer: regularizers.l2({ l2: l2Reg }),
                activation: 'tanh'
            }));

            model.add(layers.batchNormalization());
        }

        model.add(layers.dense({
            units: 1,
            kernelRegularizer: regularizers.l2({ l2: l2Reg }),
            activation: 'linear'
        }));

        return model;
    }

    private compileQFunction(): void {
        this.qFunction.compile({
            optimizer: 'adam',
            loss: losses.meanSquaredError,
            metrics: ['mae']
        });
    }

    public async fit(state: Tensor2D, action: Tensor2D, Y: Tensor<Rank>, epochs = 5000): Promise<void> {
        let X = concat([state, action], 1);
        const earlyStop = new EarlyStopping({
            monitor: 'val_loss',
            patience: 500,
            verbose: 0,
        });
        const callbacksList = [earlyStop];

        const validationCount = Math.floor(X.shape[0] * 0.2);
        const validationSet = MathHelper.validationSplit(X.shape[0], validationCount > 0 ? validationCount : 1);
        const trainingSet = MathHelper.sliceIndices(0, Y.shape[0], validationSet);
        const xValidation = X.gather(tensor1d(validationSet, 'int32'), 0);
        const yValidation = Y.gather(tensor1d(validationSet, 'int32'));

        X = X.gather(tensor1d(trainingSet, 'int32'), 0);
        const newY = Y.gather(tensor1d(trainingSet, 'int32'));

        await this.qFunction.fit(X, newY, {
            epochs,
            validationData: [xValidation, yValidation],
            callbacks: callbacksList,
            verbose: 1,
            batchSize: 1000
        });
        this.unfitted = false;
    }

    public predict(state: Tensor<Rank>, action: Tensor<Rank> | number): Tensor<Rank> | Tensor<Rank>[] {
        let qsa: Tensor<Rank> | Tensor<Rank>[];
        let X: Tensor<Rank>;
        const a = action instanceof Tensor ? action : tensor1d([action]);

        if (state.shape.length === 1) {
            X = concat([state, a], 0);
        }else {
            X = concat([state, a], 1);
        }

        if (this.unfitted) {
            if (X.shape.length === 1) {
                qsa = tensor1d([0]);
            }else {
                qsa = zeros([state.shape[0], 1]);
            }
        } else {
            if (X.shape.length === 1) {
                X = X.as2D(1, X.shape[0]);
            }
            qsa = this.qFunction.predict(X);
        }

        return qsa;
    }
}
