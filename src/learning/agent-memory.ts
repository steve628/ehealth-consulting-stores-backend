import {
    Tensor1D,
    tensor1d,
    tensor2d,
    any
} from '@tensorflow/tfjs-node';
import { writeFileSync, readFileSync } from 'fs';
import * as Deque from 'collections/deque';
import { MathHelper } from '../helper/math-helper';
import { SAVE_FOLDER_NAME } from '../config/env.config';

export class AgentMemory {
    private state: Tensor1D = null;
    private action: number = Number.NaN;
    private reward: number = Number.NaN;
    private nextState: Tensor1D = null;
    public buffer = new Deque();

    public set Reward(val: number) {
        this.reward = val;
    }

    public get Reward(): number {
        return this.reward;
    }

    public get State(): Tensor1D {
        return this.state;
    }

    public set Action(val: number) {
        this.action = val;
    }

    public get Action(): number {
        return this.action;
    }

    public addState(state: Tensor1D): void {
        this.nextState = state;
        this.addMemory();
        this.state = state;
        this.nextState = null;
    }

    private addMemory(): boolean {
        if(!this.state || !this.nextState || Boolean(any(this.state.isNaN()).arraySync())
            || Boolean(any(this.nextState.isNaN()).arraySync()) || isNaN(this.action) || isNaN(this.reward)) {
           return false;
        } else {
            const newMemory = [this.state.arraySync(), this.action, this.reward, this.nextState.arraySync()];
            this.buffer.push(newMemory);
            return true;
        }
    }

    public clearCurrentMemory(): void {
        this.state = null;
        this.nextState = null;
        this.action = Number.NaN;
        this.reward = Number.NaN;
    }

    public getBatch(size?: number) {
        let batchSize = size;
        if (batchSize === undefined || batchSize === null || batchSize > this.buffer.length) {
            batchSize = this.buffer.length;
        }
        const idx = MathHelper.validationSplit(this.buffer.length, batchSize);
        const buffer = [];
        const states = [];
        const actions = [];
        const reward = [];
        const nextStates = [];
        for (const i of idx) {
            buffer.push(this.buffer[i]);
        }

        for (const sample of buffer) {
            states.push(sample[0]);
            actions.push(sample[1]);
            reward.push(sample[2]);
            nextStates.push(sample[3]);
        }

        return {
            state: tensor2d(states),
            action: tensor1d(actions).as2D(actions.length, 1),
            reward: tensor1d(reward).as2D(reward.length, 1),
            nextState: tensor2d(nextStates)
        };
    }

    public saveMemory(name: string): void {
        try {
            writeFileSync(`./${SAVE_FOLDER_NAME}/${name}.json`, JSON.stringify(this.buffer.toJSON()));
        }catch (e) {
            console.log('Could not write file');
        }
    }

    public loadMemory(name: string): void {
        try {
           const data = readFileSync(`./${SAVE_FOLDER_NAME}/${name}.json`);
           this.buffer = new Deque(JSON.parse(data.toString()));

        } catch (e) {
            console.log(`No file of name ${name}.json found`);
        }
    }
}
