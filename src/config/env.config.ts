import * as env from 'env-var';

const TOKEN_SECRET = env.get('TOKEN_SECRET').asString();
const TOKEN_EXPIRY_TIME = env.get('TOKEN_EXPIRY_TIME').asString();
const EMAIL_SERVICE = env.get('EMAIL_SERVICE').asString();
const EMAIL_PORT = env.get('EMAIL_PORT').asPortNumber();
const EMAIL_CIPHERS = env.get('EMAIL_CIPHERS').asString();
const EMAIL_REJECT = env.get('EMAIL_REJECT').asBool();
const USER_EMAIL = env.get('USER_EMAIL').asString();
const PASSWORD_EMAIL = env.get('PASSWORD_EMAIL').asString();
const EMAIL_ADDRESS = env.get('EMAIL_ADDRESS').asString();
const VERIFICATIONCODELENGTH = env.get('VERIFICATIONCODELENGTH').asIntPositive();
const SERVER_PORT = env.get('SERVER_PORT').asPortNumber();
const LOGIN_LINK = env.get('LOGIN_LINK').asString();
const SAVE_FOLDER_NAME = env.get('SAVE_FOLDER_NAME').asString();
const SAVE_FILE_NAME = env.get('SAVE_FILE_NAME').asString();
const GAMMA = env.get('GAMMA').asFloat();
const TAU = env.get('TAU').asFloat();
const ALPHA = env.get('ALPHA').asFloat();
const ALPHA_LOW = env.get('ALPHA_LOW').asFloat();
const ALPHA_HIGH = env.get('ALPHA_HIGH').asFloat();
const EPS = env.get('EPS').asFloat();
const EPS_LOW = env.get('EPS_LOW').asFloat();
const DELTA = env.get('DELTA').asFloat();

export {
    TOKEN_SECRET,
    TOKEN_EXPIRY_TIME,
    EMAIL_ADDRESS,
    EMAIL_CIPHERS,
    EMAIL_PORT,
    EMAIL_REJECT,
    EMAIL_SERVICE,
    USER_EMAIL,
    PASSWORD_EMAIL,
    VERIFICATIONCODELENGTH,
    SERVER_PORT,
    LOGIN_LINK,
    SAVE_FOLDER_NAME,
    SAVE_FILE_NAME,
    GAMMA,
    TAU,
    ALPHA,
    ALPHA_LOW,
    ALPHA_HIGH,
    EPS,
    EPS_LOW,
    DELTA
}
