export class Converter {
    // maximum value of z * 25 (122 * 25)
    private static readonly maxIdValue = 3050;
    private static readonly maxLength = 25;
    private static readonly minValue = 48 * Converter.maxLength;
    private static readonly maxValue = 122 * Converter.maxLength;

    public static convertIdToNumber(id: string): number {
        let res = 0;
        for (const elem of id) {
            res += elem.charCodeAt(0);
        }
        return Converter.rescale(res);
        /* console.log('Res: ', res);
        res = Converter.normalize(res);
        console.log('Value: ', res);
        return res; */
    }

    public static normalize(arg: number): number {
        return arg && arg !== 0 ? (1 / this.maxIdValue) * arg : 0;
    }

    private static rescale(arg: number): number {
        let value = (arg - Converter.minValue) / (Converter.maxValue - Converter.minValue);
        return value;
    }
}
