export class MathHelper {
    public static randomChoise(p: number[]): number {
        let rnd = p.reduce((a, b) => a + b) * Math.random();
        return p.findIndex(a => (rnd -= a) < 0);
    }

    public static randomChoises(p: number[], count: number): number[] {
        return Array.from(Array(count), this.randomChoise.bind(null, p));
    }

    public static validationSplit(max: number, count: number): number [] {
        const validation: number[] = [];
        for (let i = 0; i < count; i++) {
            validation.push(Math.floor(Math.random() * max));
        }
        return validation;
    }

    public static sliceIndices(start: number, stop: number, slice: number[]): number[] {
        const training: number[] = [];
        for (let i = start; i < stop; i++) {
            if (!slice.includes(i)) {
                training.push(i);
            }
        }
        return training;
    }
}
