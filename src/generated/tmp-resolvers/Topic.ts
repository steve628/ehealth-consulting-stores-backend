// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { TopicResolvers } from "../graphqlgen";

export const Topic: TopicResolvers.Type = {
  ...TopicResolvers.defaultResolvers,

  services: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  users: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
