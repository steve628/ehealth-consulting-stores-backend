// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ServiceResolvers } from "../graphqlgen";

export const Service: ServiceResolvers.Type = {
  ...ServiceResolvers.defaultResolvers,

  subCategory: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  topics: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  favoredUsers: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  owner: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
