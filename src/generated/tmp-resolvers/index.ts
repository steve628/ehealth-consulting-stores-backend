// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { Resolvers } from "../graphqlgen";

import { Query } from "./Query";
import { AuthPayload } from "./AuthPayload";
import { User } from "./User";
import { Service } from "./Service";
import { SubCategory } from "./SubCategory";
import { Category } from "./Category";
import { Topic } from "./Topic";
import { Review } from "./Review";
import { Message } from "./Message";
import { Chat } from "./Chat";
import { Mutation } from "./Mutation";

export const resolvers: Resolvers = {
  Query,
  AuthPayload,
  User,
  Service,
  SubCategory,
  Category,
  Topic,
  Review,
  Message,
  Chat,
  Mutation
};
