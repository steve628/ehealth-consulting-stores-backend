// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { QueryResolvers } from "../graphqlgen";

export const Query: QueryResolvers.Type = {
  ...QueryResolvers.defaultResolvers,
  login: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  checkEmailAddress: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  category: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  categories: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  service: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  services: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  subCategory: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  subCategories: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  topic: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  topics: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  user: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  searchConsultants: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  startSearchSession: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  continueSearchSession: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  cancelSearchSession: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  successSearchSession: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
