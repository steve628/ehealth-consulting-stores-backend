// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { UserResolvers } from "../graphqlgen";

export const User: UserResolvers.Type = {
  ...UserResolvers.defaultResolvers,

  services: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  topics: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  writtenReviews: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  ratedReviews: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  messages: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  favoriteConsultants: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  favoriteServices: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  consultants: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
