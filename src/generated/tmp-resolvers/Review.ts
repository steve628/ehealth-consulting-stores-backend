// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ReviewResolvers } from "../graphqlgen";

export const Review: ReviewResolvers.Type = {
  ...ReviewResolvers.defaultResolvers,

  author: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  ratedConsultant: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
