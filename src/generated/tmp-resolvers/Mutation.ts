// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { MutationResolvers } from "../graphqlgen";

export const Mutation: MutationResolvers.Type = {
  ...MutationResolvers.defaultResolvers,
  registrate: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  updateUserSettings: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  createService: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  updateService: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  createCategory: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  createSubCategory: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  },
  createTopic: (parent, args, ctx) => {
    throw new Error("Resolver not implemented");
  }
};
