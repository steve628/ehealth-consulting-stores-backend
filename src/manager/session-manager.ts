import { SearchEngineAgent } from '../learning/agent';
import {
    gamma,
    tau,
    alpha,
    eps,
    networkArc
} from '../learning/learning-paramter';
import { getAllActionValues } from '../learning/action';
import {
    Rank,
    Tensor
} from '@tensorflow/tfjs-node';
import { SAVE_FILE_NAME } from '../config/env.config';

export class SessionManager {
    private static marvin: SearchEngineAgent;

    private static getAction(observation: Tensor<Rank.R1>, reward: number): number {
        this.marvin.setState(observation);
        this.marvin.setReward(reward);
        return this.marvin.getAction(observation);
    }

    public static async startSession(observation: Tensor<Rank.R1>, reward: number): Promise<number> {
        if (!this.marvin) {
            this.marvin = new SearchEngineAgent(
                observation.size,
                getAllActionValues(),
                gamma,
                eps,
                tau,
                alpha,
                undefined,
                networkArc
            );
            await this.marvin.restore(SAVE_FILE_NAME);
        }
        this.marvin.resetMemory();
        return this.getAction(observation, reward);
    }

    public static continueSession(observation: Tensor<Rank.R1>, reward: number): number {
        return this.getAction(observation, reward);
    }

    public static async endSession(observation: Tensor<Rank.R1>, reward: number): Promise<void> {
        this.marvin.setState(observation);
        this.marvin.setReward(reward);
        this.marvin.adjustLearning(reward);
        await this.marvin.learn();
        await this.marvin.save(SAVE_FILE_NAME);
    }
}
