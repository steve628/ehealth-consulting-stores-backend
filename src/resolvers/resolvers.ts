import {
    Resolvers,
    QueryResolvers,
    MutationResolvers,
    //SubscriptionResolvers
} from '../generated/graphqlgen';
import { NestedFields } from './nested/nestedFields';
import { AuthPayload } from '../generated/tmp-resolvers/AuthPayload';
import {
    UserQueryResolver,
    UserMutationResolver
} from './user.resolver';
import {
    ServiceMutationResolver,
    ServiceQueryResolver
} from './service.resolver';
import {
    CategoryMutationResolver,
    CategoryQueryResolver
} from './category.resolver';
import {
    SubCategoryMutationResolver,
    SubCategoryQueryResolver
} from './subCategory.resolver';
import {
    TopicMutationResolver,
    TopicQueryResolver
} from './topic.resolver';
import {
    SearchQueryResolver
} from './search.resolver';

const Query: QueryResolvers.Type = {
    ...UserQueryResolver,
    ...CategoryQueryResolver,
    ...ServiceQueryResolver,
    ...SubCategoryQueryResolver,
    ...TopicQueryResolver,
    ...SearchQueryResolver
};

const Mutation: MutationResolvers.Type = {
    ...UserMutationResolver,
    ...ServiceMutationResolver,
    ...CategoryMutationResolver,
    ...SubCategoryMutationResolver,
    ...TopicMutationResolver
};

//const Subscription: SubscriptionResolvers.Type = {};

export const resolvers: Resolvers = {
    Query,
    Mutation,
    //Subscription,
    ...NestedFields,
    AuthPayload
}
