import * as jwt from 'jsonwebtoken';
import { AuthPayload } from '../types';
import { TOKEN_EXPIRY_TIME, TOKEN_SECRET } from '../config/env.config';
import { createHtml,createVerification } from '../email/mailCreator';
import { sendMail } from '../email/email';
import { generateSHA512Hash } from '../authorization/cryptography';
import {
    QueryResolvers,
    MutationResolvers,
    //SubscriptionResolvers
} from '../generated/graphqlgen';

export const UserQueryResolver: Pick<QueryResolvers.Type
    , 'login'
    | 'checkEmailAddress'
    | 'user'
    > = {
    login: async (root, { email, password }, ctx) => {
        const user = await ctx.db.user({email: email});
        if (generateSHA512Hash(password) !== user.password) { throw Error('Wrong password'); }
        if (!user.active) { throw Error('Email is not verified'); }
        const authPayload: AuthPayload = {
            user,
            token: jwt.sign({ userId: user.id }, TOKEN_SECRET, { expiresIn: TOKEN_EXPIRY_TIME })
        };
        return authPayload;
    },

    checkEmailAddress: async (root, { email }, ctx) => {
        return !!(await ctx.db.user({ email }));
    },

    user: (root, args, ctx) => {
        const userId = ctx.userId;
        return ctx.db.user({ id: userId });
    }
};

export const UserMutationResolver: Pick<MutationResolvers.Type
    , 'registrate'
    | 'updateUserSettings'
    > = {
    registrate: async (root, { data }, ctx) => {
        const verificationCode = createVerification();
        const newUser = await ctx.db.createUser({
            ...data,
            password: generateSHA512Hash(data.password),
            verificationCode: verificationCode
        });
        const html = createHtml(ctx.req.protocol, ctx.req.get('host'), newUser, verificationCode);
        sendMail(newUser.email, 'Please confirm your Email account', html);
        return newUser;
    },

    updateUserSettings: (root, { data }, ctx) => {
        const userId = ctx.userId;
        return ctx.db.updateUser({
            // @ts-ignore
            data: {
                ...data,
                password: data.password ? generateSHA512Hash(data.password) : undefined,
            },
            where: { id: userId }
        });
    }
};
