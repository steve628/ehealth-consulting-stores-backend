import { QueryResolvers, MutationResolvers } from '../generated/graphqlgen';

export const SubCategoryQueryResolver: Pick<QueryResolvers.Type
    , 'subCategory'
    | 'subCategories'
    > = {
    subCategory: (root, { id }, ctx) => {
        return ctx.db.subCategory({ id });
    },
    subCategories: (root, args, ctx) => {
        return ctx.db.subCategories(args);
    }
};

export const SubCategoryMutationResolver: Pick<MutationResolvers.Type
    , 'createSubCategory'
    > = {
    createSubCategory: async (root, { name, categoryId }, ctx) => {
        const userId = ctx.userId;
        const user = await ctx.db.user({ id: userId });
        if (user.role === 'USER') { throw Error('Only Consultants can create SubCategories!'); }
        return ctx.db.createSubCategory({
            name,
            category: { connect: { id: categoryId } }
        });
    }
};
