import {
    QueryResolvers,
    MutationResolvers,
    UserOrderByInput
} from '../generated/graphqlgen';
import { Action } from '../learning/action';
import { Reward } from '../learning/reward';
import { Converter } from '../helper/converter';
import { SessionManager } from '../manager/session-manager';
import { tensor1d } from '@tensorflow/tfjs-node';

export const SearchQueryResolver: Pick<QueryResolvers.Type
    , 'searchConsultants'
    | 'startSearchSession'
    | 'continueSearchSession'
    | 'cancelSearchSession'
    | 'successSearchSession'
    > = {
    searchConsultants: (parent, { data }, ctx) => {
        throw Error('Not yet implemented');
    },
    startSearchSession: async (parent, { data }, ctx) => {
        const user = ctx.userId;
        const idx = (
            await ctx.db
                .topic({ name: data.topic })
                .users({ where: { role: "CONSULTANT" }, skip: data.skip, first: data.limit })
        ).map(c => c.id);
        const action = await SessionManager.startSession(tensor1d(idx.map(Converter.convertIdToNumber)), Reward.CONTINUE);
        return ctx.db.users({ where: { id_in: idx }, orderBy: Action[action] as UserOrderByInput });
    },
    continueSearchSession: async (parent, { data }, ctx) => {
        const user = ctx.userId;
        const idx = (
            await ctx.db
                .topic({ name: data.topic })
                .users({ where: { role: "CONSULTANT" }, skip: data.skip, first: data.limit })
        ).map(c => c.id);
        const normalIdx = idx.map(Converter.convertIdToNumber);
        if (normalIdx.length < data.limit) {
            for (let i = normalIdx.length; i < data.limit; i++) {
                normalIdx.push(0);
            }
        }
        const action = SessionManager.continueSession(tensor1d(normalIdx), Reward.CONTINUE);
        return ctx.db.users({ where: { id_in: idx}, orderBy: Action[action] as UserOrderByInput });
    },
    cancelSearchSession: async (parent, { data }, ctx) => {
        const user = ctx.userId;
        const idx = (
            await ctx.db
            .topic({ name: data.topic })
            .users({ where: { role: "CONSULTANT" }, skip: data.skip, first: data.limit })
        ).map(c => c.id);
        const normalIdx = idx.map(Converter.convertIdToNumber);
        if (normalIdx.length < data.limit) {
            for (let i = normalIdx.length; i < data.limit; i++) {
                normalIdx.push(0);
            }
        }
        SessionManager.endSession(tensor1d(normalIdx), Reward.FAIL)
            .then(() => console.log('Done training'));
        console.log('Done training');
        return true;
    },
    successSearchSession: async (parent, { data }, ctx) => {
        const user = ctx.userId;
        const idx = (
            await ctx.db
                .topic({ name: data.topic })
                .users({ where: { role: "CONSULTANT" }, skip: data.skip, first: data.limit })
        ).map(c => c.id);
        const normalIdx = idx.map(Converter.convertIdToNumber);
        if (normalIdx.length < data.limit) {
            for (let i = normalIdx.length; i < data.limit; i++) {
                normalIdx.push(0);
            }
        }
        SessionManager.endSession(tensor1d(normalIdx), Reward.SUCCESS)
            .then(() => console.log('Done training'));
        console.log('Done training');
        return true;
    }
};
