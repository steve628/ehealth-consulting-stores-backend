import { Category } from './category';
import { Chat } from './chat';
import { Message } from './message';
import { Review } from './review';
import { Service } from './service';
import { SubCategory } from './subCategory';
import { Topic } from './topic';
import { User } from './user';

export const NestedFields = {
    Category,
    Chat,
    Message,
    Review,
    Service,
    SubCategory,
    Topic,
    User
};