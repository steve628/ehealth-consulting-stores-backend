import { ServiceResolvers } from '../../generated/graphqlgen';

export const Service: ServiceResolvers.Type = {
    ...ServiceResolvers.defaultResolvers,
    subCategory: (parent, _args, ctx) => {
        return ctx.db.service({id: parent.id}).subCategory();
    },
    topics: (parent, _args, ctx) => {
        return ctx.db.service({id: parent.id}).topics();
    },
    favoredUsers: (parent, args, ctx) => {
        return ctx.db.service({id: parent.id}).favoredUsers();
    },
    owner: (parent, _args, ctx) => {
        return ctx.db.service({id: parent.id}).owner();
    }
};
