import { ChatResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Chat: ChatResolvers.Type = {
    ...ChatResolvers.defaultResolvers,
    messages: (parent, args, ctx) => {
        return ctx.db.chat({id: parent.id}).messages();
    }
};