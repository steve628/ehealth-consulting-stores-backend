import { SubCategoryResolvers } from '../../generated/graphqlgen';

export const SubCategory: SubCategoryResolvers.Type = {
    ...SubCategoryResolvers.defaultResolvers,
    category: (parent, args, ctx) => {
        return ctx.db.subCategory({id: parent.id}).category();
    },
    services: (parent, args, ctx) => {
        return ctx.db.subCategory({id: parent.id}).services();
    }
};