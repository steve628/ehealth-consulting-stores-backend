import { TopicResolvers } from '../../generated/graphqlgen';

export const Topic: TopicResolvers.Type = {
    ...TopicResolvers.defaultResolvers,
    services: (parent, args, ctx) => {
        return ctx.db.topic({id: parent.id}).services();
    },
    users: (parent, args, ctx) => {
        return ctx.db.topic({id: parent.id}).users();
    }
};