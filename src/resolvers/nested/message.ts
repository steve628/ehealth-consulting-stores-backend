import { MessageResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Message: MessageResolvers.Type = {
    ...MessageResolvers.defaultResolvers,
    chat: (parent, args, ctx) => {
        return ctx.db.message({id: parent.id}).chat();
    },
    author: (parent, args, ctx) => {
        return ctx.db.message({id: parent.id}).author();
    }
};