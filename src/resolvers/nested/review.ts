import { ReviewResolvers } from '../../generated/graphqlgen';

export const Review: ReviewResolvers.Type = {
    ...ReviewResolvers.defaultResolvers,
    author: (parent, args, ctx) => {
        return ctx.db.review({id: parent.id}).author();
    },
    ratedConsultant: (parent, args, ctx) => {
        return ctx.db.review({id: parent.id}).ratedConsultant();
    }
};