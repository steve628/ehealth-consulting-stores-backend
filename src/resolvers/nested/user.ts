import { UserResolvers } from '../../generated/graphqlgen';

export const User: UserResolvers.Type = {
    ...UserResolvers.defaultResolvers,
    services: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).services();
    },
    topics: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).topics();
    },
    writtenReviews: (parent, _rgs, ctx) => {
        return ctx.db.user({id: parent.id}).writtenReviews();
    },
    ratedReviews: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).ratedReviews();
    },
    messages: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).messages();
    },
    favoriteConsultants: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).favoriteConsultants();
    },
    favoriteServices: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).favoriteServices();
    },
    consultants: (parent, args, ctx) => {
        return ctx.db.user({id: parent.id}).consultants();
    }
};
