import { CategoryResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';


export const Category: CategoryResolvers.Type = {
    ...CategoryResolvers.defaultResolvers,
    subCategories: (parent, args, ctx) => {
        return ctx.db.category({id: parent.id}).subCategories();
    }
};