import { QueryResolvers, MutationResolvers } from '../generated/graphqlgen';

export const TopicQueryResolver: Pick<QueryResolvers.Type
    , 'topic'
    | 'topics'
    > = {
    topic: (root, { id }, ctx) => {
        return ctx.db.topic({ id });
    },
    topics: (root, args, ctx) => {
        return ctx.db.topics(args);
    }
};

export const TopicMutationResolver: Pick<MutationResolvers.Type
    , 'createTopic'
    > = {
    createTopic: async (root, { data }, ctx) => {
        const userId = ctx.userId;
        const user = await ctx.db.user({ id: userId });
        if (user.role === 'USER') { throw Error('Only Consultants can create a Topic!'); }
        return ctx.db.createTopic({
            ...data,
            services: { connect: { id: data.serviceId } },
            users: { connect: { id: data.userId } }
        });
    }
};
