import { QueryResolvers, MutationResolvers } from '../generated/graphqlgen';
import { ServiceCreateInput, ServiceUpdateInput } from '../generated/prisma-client';

export const ServiceQueryResolver: Pick<QueryResolvers.Type
    , 'service'
    | 'services'
    > = {
    service: (root, { id }, ctx) => {
        return ctx.db.service({ id });
    },

    services: (root, args, ctx) => {
        return ctx.db.services(args);
    }
};

export const ServiceMutationResolver: Pick<MutationResolvers.Type
    , 'createService'
    | 'updateService'
    > = {
    createService: async (root, { data }, ctx) => {
        const userId = ctx.userId;
        const user = await ctx.db.user({id: userId});
        if (user.role === 'USER') { throw Error('Only Consultants can create Services!'); }
        const input: ServiceCreateInput = {
            ...data,
            subCategory: { connect: { id: data.subCategoryId } },
            owner: { connect: { id: userId } }
        };
        delete input['subCategoryId'];
        return ctx.db.createService(input);
    },

    updateService: async (root, { data, serviceId }, ctx) => {
        const userId = ctx.userId;
        const exists = await ctx.db.$exists.service({ id: serviceId, owner: { id: userId } });
        if (!exists) { throw Error('You are not permitted to edit this service'); }
        return ctx.db.updateService({
            // @ts-ignore
            data: { ...data },
            where: { id: serviceId }
        });
    }
};
