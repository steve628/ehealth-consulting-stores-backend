import { QueryResolvers, MutationResolvers } from '../generated/graphqlgen';

export const CategoryQueryResolver: Pick<QueryResolvers.Type
    , 'category'
    | 'categories'
    > = {
    category: (root, { id }, ctx) => {
        return ctx.db.category({ id });
    },
    categories: (root, args, ctx) => {
        return ctx.db.categories(args);
    }
}

export const CategoryMutationResolver: Pick<MutationResolvers.Type
    , 'createCategory'
    > = {
    createCategory: async (root, { name }, ctx) => {
        const userId = ctx.userId;
        const user = await ctx.db.user({ id: userId });
        if (user.role === 'USER') { throw Error('Only Consultants can create Categories!'); }
        return ctx.db.createCategory({ name });
    }
};
