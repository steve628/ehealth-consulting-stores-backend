import { User } from '../generated/prisma-client';
import { LOGIN_LINK } from '../config/env.config';

export const renderVerifiedEmail = (user: User): string => {
    return `
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="utf-8">
                <title>Email successfully verified</title>
            </head>
            <body>
                <script>
                    (function(){
                        setTimeout(function(){ 
                            window.location.href="${LOGIN_LINK}"
                        }, 3000);
                    })()
                </script>
                <div>
                    <p>Ihr Account mit der Email-Adresse: ${user.email}, wurde erfolgreich aktiviert.</p>
                    <p>Sie werden in drei Sekunden zur Anmeldung weitergeleitet.</p>
                </div>
            </body>
        </html>
    `;
};
