import { Router } from "express";
import { verifyEmail } from "./controller";

// @ts-ignore
const mailRouter = new Router();

mailRouter.get("/verify", verifyEmail);

export { mailRouter };