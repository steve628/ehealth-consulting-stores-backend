import { VERIFICATIONCODELENGTH } from '../config/env.config';
import { Alphabet } from './alphabet';
import { User } from "../generated/prisma-client";

export const createVerification = () => {
    let code = '';
    for (let i = 0; i < VERIFICATIONCODELENGTH; i++) {
        code += Alphabet.charAt(Math.random() * Alphabet.length);
    }
    return code;
};

const createLink = (protocol: string, host: string, userid: string, verification: string) => {
    return protocol + '://' + host + '/email/verify?id=' + verification + '&user=' + userid;
};

export const createHtml = (protocol: string, host: string, user: User, verification: string) => {
    const forename = (user.forename) ? user.forename : 'UserIn';
    const lastName = (user.lastname) ? user.lastname : '';
    return `Hello ${forename} ${lastName}<br>Please click on the link to verify your email<br>
            <a href=${createLink(protocol, host, user.id, verification)}>Click here to verify</a>`;
};
