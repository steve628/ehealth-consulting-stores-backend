import { prisma, User } from '../generated/prisma-client';
import { renderVerifiedEmail } from './email.template';

export const verifyEmail = async (request, response) => {
    if (request.query.user) {
        const user: User = await prisma.user({id: request.query.user});
        if (user && user.verificationCode && request.query.id === user.verificationCode) {
            console.log('Email is now verified');
            await  prisma.updateUser({data: {active: true}, where: {id: user.id}});
            // response.end(`<h1>E-Mail address: ${user.email} successfully verified</h1>`);
            response.end(renderVerifiedEmail(user));
        } else {
            console.log('Email can not be verified');
            response.end('<h1>Bad request</h1>');
        }
    } else {
        console.log('No user in request found');
        response.end('<h1>Bad request</h1>');
    }
}
