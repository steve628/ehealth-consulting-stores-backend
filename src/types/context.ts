import { Prisma } from "../generated/prisma-client";
import { Request } from 'express';

export interface Context {
    db: Prisma;
    req: Request;
    userId: string;
}